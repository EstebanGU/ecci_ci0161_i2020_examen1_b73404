package cql.ecci.ucr.ac.cr.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<TableTop> juegos = new ArrayList<TableTop>();
    private ListView listaJuegos;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Leer juegos de la BD
        leerJuegos();
        // Rellenar la lista de juegos con los juegos leidos de la base
        listaJuegos = (ListView) findViewById(R.id.listaJuegos);
        TableTopAdapter countriesAdapter = new TableTopAdapter(this, this.juegos);
        listaJuegos.setAdapter(countriesAdapter);

        // On click listener para mostrar detalles
        listaJuegos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int posicion, long id) {
                // ListView Clicked item value
                TableTop item = (TableTop) listaJuegos.getItemAtPosition(posicion);
                // Llamar actividad pasando objecto por parámetro
                Intent intent = new Intent(getApplicationContext(), TableTopDetalles.class);
                intent.putExtra("TableTop", item);
                startActivity(intent);
            }
        });

    }

    // Lectura de los juegos de la base de datos, es necesario instanciar un TableTop porque este método
    private void leerJuegos(){
        this.juegos = TableTop.leerJuegos(getApplicationContext());
    }
}
