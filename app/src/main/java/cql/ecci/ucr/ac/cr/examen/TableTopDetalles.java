package cql.ecci.ucr.ac.cr.examen;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

public class TableTopDetalles extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.juego_detalles);
        final TableTop juego = getIntent().getExtras().getParcelable("TableTop");
        // Agregar fragmento
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentDetalles fragment = FragmentDetalles.newInstance(juego);
        fragmentTransaction.replace(R.id.detallesFragmento, fragment);
        fragmentTransaction.commit();
    }
}
