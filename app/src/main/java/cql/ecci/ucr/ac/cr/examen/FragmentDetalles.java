package cql.ecci.ucr.ac.cr.examen;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentDetalles extends Fragment {
    public FragmentDetalles() {
        // Required empty public constructor
    }

    public static FragmentDetalles newInstance(TableTop juego) {
        FragmentDetalles fragment = new FragmentDetalles();
        Bundle args = new Bundle();
        args.putParcelable("TableTop", juego);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;
        rootView = inflater.inflate(R.layout.fragment_detalles, container, false);
        final TableTop juego = (TableTop) getArguments().getParcelable("TableTop");
        // Rellear el layout con informacion del juego obtenido por parámetro
        // Imagen
        ImageView imagen = rootView.findViewById(R.id.img);
        String nombreImagen = juego.getImagenJuego(juego.getName());
        int countryImage = getActivity().getApplicationContext().getResources().getIdentifier(nombreImagen, "drawable", getActivity().getApplicationContext().getPackageName());
        imagen.setImageResource(countryImage);
        // Id
        TextView id = (TextView) rootView.findViewById(R.id.id);
        id.setText(juego.getId());
        // Nombre
        TextView nombre = (TextView) rootView.findViewById(R.id.name);
        nombre.setText(juego.getName());
        // Pais
        TextView pais = (TextView) rootView.findViewById(R.id.country);
        pais.setText(juego.getCountry());
        // Numero de jugadores
        TextView jugadores = (TextView) rootView.findViewById(R.id.noPlayers);
        jugadores.setText("No Players: " + juego.getNoPlayers());
        // Descripcion
        TextView descripcion = (TextView) rootView.findViewById(R.id.description);
        descripcion.setText(juego.getDescription());
        // Edad
        TextView edad = (TextView) rootView.findViewById(R.id.ages);
        edad.setText("Ages: " + juego.getAges());
        // Tiempo de juego
        TextView tiempoJuego = (TextView) rootView.findViewById(R.id.playingTime);
        tiempoJuego.setText("Playing time: " + juego.getPlayingTime());
        // Distribuidor - año
        TextView compania = (TextView) rootView.findViewById(R.id.publisherYear);
        compania.setText(juego.getPublisher() + "-" + juego.getYear());

        // Boton de ir al mapa
        final Button buttonIrMapa = rootView.findViewById(R.id.buttonIrMapa);
        buttonIrMapa.setOnClickListener(new View.OnClickListener() {
            // On click para llevarlo al mapa
            public void onClick(View v) {
                // Lanza intent a la actividad de mapas
                Intent mapaIntent = new Intent(getActivity(), MapaActivity.class);
                mapaIntent.putExtra("TableTop", juego);
                startActivity(mapaIntent);
            }
        });
        return rootView;
    }
}
