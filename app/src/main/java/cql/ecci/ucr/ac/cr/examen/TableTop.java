package cql.ecci.ucr.ac.cr.examen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class TableTop implements Parcelable {

    public static final String PLAYINGTIME_HOURS="hours";
    public static final String PLAYINGTIME_MINUTES="minutes";
    public static final String PLAYINGTIME_VARIES="varies";

    private String id;
    private String name;
    private String year;
    private String publisher;
    private String country;
    private double latitude;
    private double longitude;
    private String description;
    private String noPlayers;
    private String ages;
    private String playingTime;

    public TableTop(){}

    public TableTop(String id, String name, String year, String publisher, String country, double latitude, double longitude, String description, String noPlayers, String ages, String playingTime) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.publisher = publisher;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.noPlayers = noPlayers;
        this.ages = ages;
        this.playingTime = playingTime;
    }

    protected TableTop(Parcel in) {
        id = in.readString();
        name = in.readString();
        year = in.readString();
        publisher = in.readString();
        country = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        description = in.readString();
        noPlayers = in.readString();
        ages = in.readString();
        playingTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(year);
        dest.writeString(publisher);
        dest.writeString(country);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(description);
        dest.writeString(noPlayers);
        dest.writeString(ages);
        dest.writeString(playingTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    // Getters y Setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNoPlayers() {
        return noPlayers;
    }

    public void setNoPlayers(String noPlayers) {
        this.noPlayers = noPlayers;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(String playingTime) {
        this.playingTime = playingTime;
    }

    // Para insertar en la base de datos

    public void insertar(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry._ID, getId());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME, getName());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR, getYear());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER, getPublisher());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY, getCountry());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE, getLatitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE, getLongitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION, getDescription());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOPLAYERS, getNoPlayers());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES, getAges());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYINGTIME, getPlayingTime());
        db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, null, values);
    }

    // Lectura de juegos de la base de datos y retorna un array de objetos tipo TableTop
    // estatico para poder llamarlo sin instanciar un objeto innecesario en la MainActivity

    public static ArrayList<TableTop> leerJuegos(Context context) {
        ArrayList<TableTop> juegos = new ArrayList<TableTop>();

        String selectQuery = "SELECT * FROM " + DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP;

        // SELECT de la base de datos en ejecucion
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Loop de todas las filas y agregar a lista
        if (cursor.moveToFirst()) {
            do {
                TableTop juego = new TableTop();
                // Rellenar la informacion del juego
                juego.setId(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry._ID)));
                juego.setName(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME)));
                juego.setYear(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR)));
                juego.setPublisher(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER)));
                juego.setCountry((cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY))));
                juego.setLatitude(Double.parseDouble(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE))));
                juego.setLongitude(Double.parseDouble(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE))));
                juego.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION)));
                juego.setNoPlayers(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOPLAYERS)));
                juego.setAges(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES)));
                juego.setPlayingTime(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYINGTIME)));
                juegos.add(juego);
            } while (cursor.moveToNext());
        }
        return juegos;
    }

    // Para retornar el string del nombre del archivo de cada uno de las imagenes con su respectivo juego

    public String getImagenJuego(String nombre){
        String imagen = nombre.toLowerCase();
        // Solo estos 2 juegos tienen el nombre distinto en la imagen
        switch (imagen){
            case "eldritch horror":
                imagen = "eldritch";
                break;
            case "magic: the gathering":
                imagen = "mtg";
                break;
        }
        return imagen;
    }
}
