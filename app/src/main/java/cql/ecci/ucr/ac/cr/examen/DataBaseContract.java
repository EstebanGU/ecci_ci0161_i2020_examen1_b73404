package cql.ecci.ucr.ac.cr.examen;
import android.provider.BaseColumns;
public final class DataBaseContract {
    private DataBaseContract(){}
    public static class DataBaseEntry implements BaseColumns {
        // Clase TableTop
        public static final String TABLE_NAME_TABLETOP = "TableTop";
        //private String id; Utilizamos DataBaseEntry._ID de BaseColumns
        //private String name;
        public static final String COLUMN_NAME_NAME = "name";
        //private String year;
        public static final String COLUMN_NAME_YEAR = "year";
        //private String publisher;
        public static final String COLUMN_NAME_PUBLISHER = "publisher";
        //private String country;
        public static final String COLUMN_NAME_COUNTRY = "country";
        //private long latitude;
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        //private long longitude;
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        //private String description;
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        //private String noPlayers;
        public static final String COLUMN_NAME_NOPLAYERS = "noPLayers";
        //private String ages;
        public static final String COLUMN_NAME_AGES = "ages";
        //private String playingTime;
        public static final String COLUMN_NAME_PLAYINGTIME = "playingTime";
    }

    // Construir las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    // Tabla TABLETOP
    public static final String SQL_CREATE_TABLETOP = "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP
            + " (" + DataBaseEntry._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_YEAR  + TEXT_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_PUBLISHER  + TEXT_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_COUNTRY  + TEXT_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_LATITUDE  + REAL_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_LONGITUDE  + REAL_TYPE  + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_DESCRIPTION  + TEXT_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_NOPLAYERS  + TEXT_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_AGES  + TEXT_TYPE + COMMA_SEP
            + DataBaseEntry.COLUMN_NAME_PLAYINGTIME  + TEXT_TYPE + ")";

    public static final String SQL_DELETE_TABLETOP = "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;

}
