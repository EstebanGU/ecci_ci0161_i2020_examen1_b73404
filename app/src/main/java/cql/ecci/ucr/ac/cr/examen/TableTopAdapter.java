package cql.ecci.ucr.ac.cr.examen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TableTopAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<TableTop> juegos;

    public TableTopAdapter(Context context, ArrayList<TableTop> juegos) {
        this.context = context;
        this.juegos = juegos;
    }

    @Override
    public int getCount() {
        return juegos.size();
    }

    @Override
    public Object getItem(int position) {
        return juegos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TableTop Item = (TableTop) getItem(position);
        View rowView;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.juego_item, null);
        } else {
            rowView = convertView;
        }

        ImageView img = rowView.findViewById(R.id.img);
        TextView nombre = rowView.findViewById(R.id.name);
        TextView descripcion = rowView.findViewById(R.id.description);
        String imagen = Item.getImagenJuego(Item.getName());
        int countryImage = this.context.getApplicationContext().getResources().getIdentifier(imagen, "drawable", this.context.getPackageName());
        img.setImageResource(countryImage);
        nombre.setText(Item.getName());
        descripcion.setText(Item.getDescription());
        return rowView;
    }
}
