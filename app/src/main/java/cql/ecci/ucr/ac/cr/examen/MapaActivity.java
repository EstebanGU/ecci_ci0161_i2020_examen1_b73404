package cql.ecci.ucr.ac.cr.examen;

import androidx.fragment.app.FragmentActivity;

import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LatLng direccion; // Para la direccion de la compañia
    private TableTop juego; // Objecto pasado por parámetro

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        juego = getIntent().getExtras().getParcelable("TableTop");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Marcador en la compañia y mover la camara
        this.direccion = new LatLng(this.juego.getLatitude(), this.juego.getLongitude());
        mMap.addMarker(new MarkerOptions().position(direccion).title("Marcador en " + this.juego.getPublisher()));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(direccion));
        CameraPosition cameraPosition = new CameraPosition.Builder() .target(new LatLng(juego.getLatitude(), juego.getLongitude())).zoom(15)
        // zoom level
        .bearing(70) // bearing // direccion de la camara
        .tilt(25) // tilt angle // inclinacion
        .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
