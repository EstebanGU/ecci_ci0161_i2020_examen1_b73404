package cql.ecci.ucr.ac.cr.examen;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// Clase DataBaseHelper que hereda de SQLiteOpenHelper
public class DataBaseHelper extends SQLiteOpenHelper {

    // Cada vez que cambie el esquema de la base de datos DataBaseContract,
    // debemos incrementar la version de la base de datos
    public static final int DATABASE_VERSION = 3;

    // Nombre de la base de datos
    public static final String DATABASE_NAME = "TableTop.db";

    // constructor de la clase, el contexto tiene la informacion global sobre el ambiente de la app
    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Crear la base de datos de la app
        db.execSQL(DataBaseContract.SQL_CREATE_TABLETOP);
        // Poblar la base una única vez
        TableTop catan = new TableTop("TT001", "Catan", "1995", "Kosmos", "Germany", 48.774538, 9.188467, "Picture yourself in the era of discoveries:" +
                "after a long voyage of great deprivation," +
                "your ships have finally reached the coast of" +
                "an uncharted island. Its name shall be Catan!" +
                "But you are not the only discoverer. Other" +
                "fearless seafarers have also landed on the" +
                "shores of Catan: the race to settle the" +
                "island has begun!", "3-4", "10+", "1-2 " + TableTop.PLAYINGTIME_HOURS);
        TableTop monopoly = new TableTop("TT002", "Monopoly", "1935", "Hasbro", "United States", 41.883736, -71.352259, "The thrill of bankrupting an opponent, but it" +
                "pays to play nice, because fortunes could" +
                "change with the roll of the dice. Experience" +
                "the ups and downs by collecting property" +
                "colors sets to build houses, and maybe even" +
                "upgrading to a hotel!", "2-8", "8+", "20-180 " + TableTop.PLAYINGTIME_MINUTES);

        TableTop eldritchHorror = new TableTop("TT003", "Eldritch Horror", "2013", "Fantasy Flight Games", "United States", 45.015417, -93.183995, "An ancient evil is stirring. You are part of" +
                "a team of unlikely heroes engaged in an" +
                "international struggle to stop the gathering" +
                "darkness. To do so, you’ll have to defeat" +
                "foul monsters, travel to Other Worlds, and" +
                "solve obscure mysteries surrounding this" +
                "unspeakable horror.", "1-8", "14+", "2-4 " + TableTop.PLAYINGTIME_HOURS);

        TableTop magic = new TableTop("TT004", "Magic: the Gathering", "1993", "Hasbro", "United States", 41.883736, -71.352259, "Magic: The Gathering is a collectible and" +
                "digital collectible card game created by" +
                "Richard Garfield. Each game of Magic" +
                "represents a battle between wizards known as" +
                "planeswalkers who cast spells, use artifacts," +
                "and summon creatures.", "2+", "13+", TableTop.PLAYINGTIME_VARIES);

        TableTop hanabi = new TableTop("TT005", "Hanabi", "2010", "Asmodee", "France", 48.761629, 2.065296, "Hanabi—named for the Japanese word for" +
                "\"fireworks\"—is a cooperative game in which" +
                "players try to create the perfect fireworks" +
                "show by placing the cards on the table in the" +
                "right order.", "2-5", "8+", "25 " + TableTop.PLAYINGTIME_MINUTES);
        // Insertar Juegos
        catan.insertar(db);
        monopoly.insertar(db);
        eldritchHorror.insertar(db);
        magic.insertar(db);
        hanabi.insertar(db);
    }

    // implementamos el metodo para la actualizacion de la base de datos
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Administracion de actualizaciones
        db.execSQL(DataBaseContract.SQL_DELETE_TABLETOP);
        onCreate(db);
    }

    // inplementamos el metodo para volver a la version anterior de la base de datos
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
